.DEFAULT_GOAL := help

ifndef CONDA_PREFIX
$(error Conda not active, please install conda and then create an environment with `conda create --name nicholls-carbon-budget-2020` and activate it with `conda activate nicholls-carbon-budget-2020`))
else
ifeq ($(CONDA_DEFAULT_ENV),base)
$(error Do not install to conda base environment. Source a different conda environment e.g. `conda activate nicholls-carbon-budget-2020` or `conda create --name nicholls-carbon-budget-2020` and rerun make))
endif
endif

PYTHON=$(CONDA_PREFIX)/bin/python
CONDA_ENV_FILE=environment.yml
SCONS_FILE=SConstruct


SCRIPTS_DIR=scripts
DATA_DIR=data
NOTEBOOKS_DIR=notebooks
FIGURES_DIR=figures
OUTPUT_DIR=output
SECTIONS_DIR=sections


FILES_TO_FORMAT_PYTHON=$(SCRIPTS_DIR) $(SCONS_FILE) $(NOTEBOOKS_DIR)/*.py


MAIN_TEX_FILE_NAME=main.tex
BIBLIOGRAPHY_FILE_NAME=bibliography.bib


SR15_OPENSCM_SCENARIOS_GENERATION_SCRIPT=$(SCRIPTS_DIR)/get_sr15_scenarios.py
SR15_SCEN_FILES_DIR=$(DATA_DIR)/sr15_scenfiles/scenfiles
SR15_VALID_SCENARIOS_LIST=$(DATA_DIR)/sr15_scenfiles/final411scenarios.csv
SR15_OPENSCM_SCENARIOS_DIR=$(DATA_DIR)/sr15_scenarios
SR15_OPENSCM_SCENARIOS=$(SR15_OPENSCM_SCENARIOS_DIR)/sr15_scenarios.csv


ONLINE_BUDGET_TEMPERATURE_CUMULATIVE_CO2_FIGURE=$(FIGURES_DIR)/online_budget_temperature_cumulative_co2_LT_delta-0x40f40702_base-0x450c084a_LT.png
FIGURES = \
  $(FIGURES_DIR)/sr15-carbon-budget-framework.jpg \
  $(ONLINE_BUDGET_TEMPERATURE_CUMULATIVE_CO2_FIGURE)


MANUSCRIPT_NAME=nicholls-carbon-budgets-2020
MANUSCRIPT_OUTPUT=$(MANUSCRIPT_NAME).pdf


define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([\$$\(\)a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT


help:  ## print short description of each target
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)
	@echo ""
	@make variables

.PHONY: all
all:  $(SR15_OPENSCM_SCENARIOS)  ## make the full manuscript in pdf format
	export NOTEBOOKS_DIR=$(NOTEBOOKS_DIR); \
	export MAIN_TEX=$(SECTIONS_DIR)/$(MAIN_TEX_FILE_NAME); \
	export OUTPUT_PDF=$(MANUSCRIPT_OUTPUT); \
	scons --tree=prune,status
# 	cd $(SECTIONS_DIR); pandoc main.tex -o ../nicholls-carbon-budgets-2020.docx  # build docx with something like this

.PHONY: manuscript-only
manuscript-only:  ## make only the manuscript, ignoring dependencies
	make clean-latex
	export MANUSCRIPTONLY=True; \
	export MAIN_TEX=$(SECTIONS_DIR)/$(MAIN_TEX_FILE_NAME); \
	export OUTPUT_PDF=$(MANUSCRIPT_OUTPUT); \
	scons --tree=prune,status
# 	cd $(SECTIONS_DIR); pandoc -s main.tex --bibliography bibliography.bib --biblatex -M biblio-style=nature -o ../nicholls-carbon-budgets-2020.docx

justification-statement:  ## make the justification statement
	pandoc --filter pandoc-citeproc --bibliography justification-statement-references.bib justification-statement.md -o justification-statement.pdf \
  --pdf-engine=xelatex

cover-letter:  ## make the justification statement
	pandoc --filter pandoc-citeproc --bibliography cover-letter-references.bib cover-letter.md -o cover-letter.pdf \
  --pdf-engine=xelatex

.PHONY: wordcount
wordcount:
	cd $(SECTIONS_DIR);texcount -inc $(MAIN_TEX_FILE_NAME)

.PHONY: data
data: $(CONDA_PREFIX) $(SR15_OPENSCM_SCENARIOS)  ## collect and process all the input data required for the manuscript

$(SR15_OPENSCM_SCENARIOS): $(SR15_OPENSCM_SCENARIOS_GENERATION_SCRIPT) $(SR15_SCEN_FILES_DIR) $(SR15_VALID_SCENARIOS_LIST) $(CONDA_PREFIX)
	mkdir -p $(SR15_OPENSCM_SCENARIOS_DIR)
	$(PYTHON) $< --inputdir $(SR15_SCEN_FILES_DIR) --scenlist $(SR15_VALID_SCENARIOS_LIST) $@

.PHONY: conda-environment
conda-environment: $(CONDA_PREFIX)  ## create conda environment for generating the paper
$(CONDA_PREFIX): $(CONDA_ENV_FILE)
	$(CONDA_EXE) env update --name $(CONDA_DEFAULT_ENV) -f $(CONDA_ENV_FILE)
	touch $(CONDA_PREFIX)

.PHONY: black
black: $(CONDA_PREFIX)  ## auto-format the Python scripts
	@status=$$(git status --porcelain $(FILES_TO_FORMAT_PYTHON)); \
	if test "x$${status}" = x; then \
		$(CONDA_PREFIX)/bin/black $(FILES_TO_FORMAT_PYTHON); \
	else \
		echo Not trying any formatting. Working directory is dirty ... >&2; \
	fi

.PHONY: clean
clean:  ## remove the latex compilation byproducts and the conda environment
	make clean-latex
	make clean-conda-environment

.PHONY: clean-latex
clean-latex:  ## clean all the products of building converting latex to pdf
	find . -type f -name '*.aux' ! -path "./sections-submission/*" -delete
	find . -type f -name '*.log' ! -path "./sections-submission/*" -delete
	find . -type f -name '*.bbl' ! -path "./sections-submission/*" -delete
	find . -type f -name '*.blg' ! -path "./sections-submission/*" -delete
	find . -type f -name '*.fls' ! -path "./sections-submission/*" -delete
	find . -type f -name '*.bcf' ! -path "./sections-submission/*" -delete
	find . -type f -name '*.out' ! -path "./sections-submission/*" -delete
	find . -type f -name '*.xml' ! -path "./sections-submission/*" -delete

.PHONY: clean-conda-environment
clean-conda-environment:  ## remove the conda environment
	rm -rf $(CONDA_PREFIX)

diff.pdf: $(MANUSCRIPT_OUTPUT)  ## make pdf which shows difference between submitted and revised manuscripts
	rm $(MANUSCRIPT_OUTPUT)
	make manuscript-only
	cp main.bbl sections/
	latexdiff sections-submission/main.tex sections/main.tex --flatten > sections/diff.tex
	latexdiff sections-submission/main.bbl sections/main.bbl > sections/diff.bbl
	cd sections; pdflatex diff.tex;mv diff.pdf ../
	cd sections; pdflatex diff.tex;mv diff.pdf ../

.PHONY: variables
variables:  ## display the value of all variables in the Makefile
	@echo CONDA_PREFIX: $(CONDA_PREFIX)
	@echo PYTHON: $(PYTHON)
	@echo CONDA_DEFAULT_ENV: $(CONDA_DEFAULT_ENV)
	@echo CONDA_PREFIX: $(CONDA_PREFIX)
	@echo CONDA_ENV_FILE: $(CONDA_ENV_FILE)
	@echo SCONS_FILE: $(SCONS_FILE)
	@echo ""
	@echo DATA_DIR: $(DATA_DIR)
	@echo SCRIPTS_DIR: $(SCRIPTS_DIR)
	@echo NOTEBOOKS_DIR: $(NOTEBOOKS_DIR)
	@echo FIGURES_DIR: $(FIGURES_DIR)
	@echo OUTPUT_DIR: $(OUTPUT_DIR)
	@echo SECTIONS_DIR: $(SECTIONS_DIR)
	@echo ""
	@echo SR15_OPENSCM_SCENARIOS_GENERATION_SCRIPT: $(SR15_OPENSCM_SCENARIOS_GENERATION_SCRIPT)
	@echo SR15_SCEN_FILES_DIR: $(SR15_SCEN_FILES_DIR)
	@echo SR15_OPENSCM_SCENARIOS_DIR: $(SR15_OPENSCM_SCENARIOS_DIR)
	@echo SR15_OPENSCM_SCENARIOS: $(SR15_OPENSCM_SCENARIOS)
	@echo ""
	@echo FILES_TO_FORMAT_PYTHON: $(FILES_TO_FORMAT_PYTHON)
	@echo ""
	@echo MANUSCRIPT_NAME: $(MANUSCRIPT_NAME)
	@echo MANUSCRIPT_OUTPUT: $(MANUSCRIPT_OUTPUT)
