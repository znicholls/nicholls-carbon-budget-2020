# Only call this with `make all` to ensure the environment
# variables are correct
import os
import glob
from distutils.util import strtobool

from nbflow.scons import setup


def get_env_var_else_raise(env_var):
    res = os.getenv(env_var, None)
    if res is None:
        raise ValueError("{} is not a defined environment variable".format(env_var))
    return res

env = Environment(ENV=os.environ)

main = get_env_var_else_raise("MAIN_TEX")
output_pdf = get_env_var_else_raise("OUTPUT_PDF")
if not strtobool(os.getenv("MANUSCRIPTONLY", "False")):
    notebooks_dir = get_env_var_else_raise("NOTEBOOKS_DIR")

    setup(env, [notebooks_dir], ARGUMENTS)

manuscript_input = (
    sorted(glob.glob("sections/*.tex"))
    + sorted(glob.glob("sections/*.bib"))
    + sorted(glob.glob("figures/*.jpg"))
    + sorted(glob.glob("figures/*.pdf"))
)

if not strtobool(os.getenv("CI", "False")):
    env.PDF(output_pdf, main)
    env.Depends(output_pdf, manuscript_input)
