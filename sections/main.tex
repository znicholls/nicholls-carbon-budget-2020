% Submitted to: Environmental Research Letters
% Lead Author: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>
% Reference number:
\documentclass[12pt]{iopart}
\newcommand{\gguide}{{\it Preparing graphics for IOP Publishing journals}}
%Uncomment next line if AMS fonts required
%\usepackage{iopams}

% ensure pdfs are version1.4 or lower for compatibility reasons
\pdfminorversion=4

% figures handling package
\usepackage{graphicx}

% TODO if accepted:
%   - tidy up table to use IOP style (do this by hand), full details in Section 8 of IOP style guide document
%   - hard-code supplementary references and remove \CO2 command
%   - save figures as vectors

%%%%%%%%%%%%%%%%
%% Required packages
% english typography
\usepackage[english]{babel}

% symbols
\usepackage{gensymb}

% list control
\usepackage{enumitem}

% table stuff
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage{dcolumn}
\newcolumntype{d}[1]{D{.}{.}{#1} }

% input encodings
\usepackage[utf8]{inputenc}

% iop's version of ams maths environments
\usepackage{iopams}

% figure handling
\usepackage{graphicx}

% todo Notes
\usepackage{todonotes}

% urls
\usepackage{hyperref}
\hypersetup{pdfpagemode=UseOutlines}

% references
\usepackage[jphysicsB]{harvard}  % options: http://mirror.aarnet.edu.au/pub/CTAN/macros/latex/contrib/harvard/harvard.pdf
% this citation gives long entries for SR1.5 Ch.2, see https://latex.org/forum/viewtopic.php?t=27226
\citationmode{abbr} % https://tex.stackexchange.com/questions/434968/no-more-than-two-authors-in-citation-call-outs-when-using-harvard-citation-man

% % line numbers
% \usepackage{lineno}
% \linenumbers

%%%%%%%%%%%%%%%%%%
% Other config
% cover running from this directory and one above
% \graphicspath{ {./figures/} {../figures/} }

%%%%%%%%%%%%%%%%%%
% My commands
\newcommand{\CO}[1]{CO\textsubscript{#1}}
\newcommand{\beginsupplement}{%
        \setcounter{table}{0}
        \renewcommand{\thetable}{S\arabic{table}}%
        \setcounter{figure}{0}
        \renewcommand{\thefigure}{S\arabic{figure}}%
        \setcounter{section}{0}
        \renewcommand{\thesection}{S\arabic{section}}%
        \setcounter{equation}{0}
        \renewcommand{\theequation}{S\arabic{equation}}%
     }
%%%%%%%%%%%%%%%%%%

\begin{document}

\title{Implications of non-linearities between cumulative \CO2 emissions and \CO2-induced warming for assessing the remaining carbon budget}
% \title{Considering the impact of nonlinearities in the relationship between cumulative \CO2 emissions and warming within remaining carbon budget assessment frameworks}
% \title{A bias correction for the remaining carbon budget assessment framework}
% \title{Resolving differences between remaining carbon budget assessment frameworks}
% alternatives
% \title{Examining carbon budget assessment frameworks}
% \title{Disentangling carbon budget assessment frameworks}
%\title{Use of the transient climate response to emissions in carbon budget assessment}
% \title{Disentangling differences in Carbon Budget assessments}
% \title{On the application of the transient climate response to emissions in carbon budget assessment}

\input{authorship.tex}

\input{custom_commands_for_results.tex}
\input{custom_commands_diagnosis_runs.tex}

\begin{abstract}
\input{abstract.tex}
\end{abstract}

\noindent{\it Keywords\/}: climate change, global warming, remaining carbon budget, peak temperature

\submitto{\ERL}

\maketitle
% \ioptwocol  % toggle to check two column layout, turn line numbers off at the same time

\input{introduction.tex}

\input{methods.tex}

\input{results.tex}

\input{limitations.tex}

\input{conclusions.tex}

\ack

We could not have produced this manuscript without the feedback of Sonya Fiddes, Matthias Mengel and David Karoly.
ZN and MM would also like to thank all the participants at the International Workshop on the Remaining Carbon Budget, organised with the support of the Global Carbon Project, the CRESCENDO project, Stanford University, the University of Melbourne, and Simon Fraser University, for the insightful discussions, in particular Joeri Rogelj and Damon Matthews.

We would also like to thank two anonymous reviewers for their careful, detailed and insightful reviews.
The manuscript was greatly improved thanks to their efforts and feedback.

\section*{Funding} % (fold)
\label{sec:funding}

ZN benefited from support provided by the ARC Centre of Excellence for Climate Extremes (CE170100023).

% section funding (end)

\section*{Author Contributions} % (fold)
\label{sec:author_contributions}

ZN, RG, AN and MM designed the study.
ZN, RG, and JL performed the study.
ZN and MM designed and produced all the figures.
ZN wrote the first draft of the manuscript.
All authors contributed to the editing and revisions of the manuscript.

% section author_contributions (end)

\section*{Competing Interests} % (fold)
\label{sec:competing_interests}

The authors declare no competing interests.

% section competing_interests (end)

\section*{Data and Materials Availability} % (fold)
\label{sec:data_and_materials_availability}

The data that support the findings of this study are openly available at \url{https://doi.org/10.5281/zenodo.3726578} and at \\
\url{https://gitlab.com/znicholls/nicholls-carbon-budget-2020}.
Pymagicc v2.0.0-beta, which includes MAGICC6, is archived at \url{https://doi.org/10.5281/zenodo.3386579}.

% section data_and_materials_availability (end)

\clearpage

\input{results_overview_table.tex}

\clearpage

\input{results_components_breakdown.tex}

\clearpage

\begin{figure*}[t]
    % \includegraphics[]{zncb_online_runs_overview.jpg}
    \includegraphics[]{figure_1.jpg}
    \caption[Calculating the remaining carbon budget from model simulations with interacting climate components]{Calculating the remaining carbon budget from model simulations with interacting climate components. For each scenario assessed in SR1.5 we diagnose peak warming and compatible cumulative \CO2 emissions (\Sref{sub:integrated_calculation}). (a) Cumulative \CO2 emissions; (b) Atmospheric \CO2 concentrations; (c) Total, non-\CO2 greenhouse gas (GHG) and aerosol radiative forcing; (d) Surface temperature rise; (e) Relationship between surface temperature rise and cumulative \CO2 emissions, including a linear regression between peak warming and compatible cumulative \CO2 emissions over all scenarios. \CO2 emissions from permafrost feedbacks are included in all simulations but are not shown here (see instead Panel (l) of \Fref{fig:sr15_budget_calculation_overview}).}
    \label{fig:online_runs_overview}
\end{figure*}

\begin{figure*}[t]
    % \includegraphics[width=0.9\textwidth]{zncb_segmented_calculation_steps.jpg}
    \includegraphics[width=0.9\textwidth]{figure_2.jpg}
    \caption[SR1.5-style remaining carbon budget assessment using the segmented framework with an LCE implementation]{SR1.5-style remaining carbon budget assessment. The assessment uses the segmented framework, implemented with a linear conversion to cumulative \CO2 emissions (`segmented-LCE'). (a) Combination of the independent assessments of the climate system's components; (b)-(e) Assessment of the TCRE using a 1pctCO2 experiment; (f)-(h) Assessment of the non-CO$_2$ warming; (i)-(k) Assessment of the zero (\CO2) emissions commitment; (l) Assessment of the contribution of processes not considered in the other components (so-called `otherwise unrepresented processes'). All magnitudes are illustrative only.}
    \label{fig:sr15_budget_calculation_overview}
\end{figure*}

\begin{figure*}[t]
    % \includegraphics[]{zncb_sr15_framework_performance.jpg}
    \includegraphics[]{figure_3.jpg}
    \caption[Segmented-LCE compared to model simulations with interacting climate components]{Relationship between cumulative \CO2 emissions (rel. to 2011) and peak surface temperature (rel. to 2006-2015) calculated following the segmented-LCE implementation (red line) and model simulations with interacting climate components (blue dashed line). The grey dots represent the peak warming from each simulation, with one simulation being performed per SR1.5 scenario.}
    \label{fig:online_offline_budget_temperature_cumulative_co2}
\end{figure*}

\begin{figure*}[t]
    % \includegraphics[]{zncb_tcre_diagnosis_bias.jpg}
    \includegraphics[]{figure_4.jpg}
    \caption[Accounting for the non-linearity in the relationship between peak warming and cumulative \CO2 emissions]{
    Non-linearity in the relationship between \CO2-induced warming and cumulative \CO2 emissions.
    Panels (a) and (b) show the relationship on conventional, cumulative \CO2 emissions--\CO2-induced warming axes.
    Panels (c) and (d) show the residuals from the assumption of strict linearity, based on the TCRE derived from a 1pctCO2 experiment.
    Panel (a) shows warming relative to pre-industrial while panel (b) shows warming relative to 2006-2015.
    Given that the residuals calculated in panels (c) and (d) are derived from panels (a) and (b) respectively, the difference between panels (c) and (d) is due to the different reference periods.
    The assumption of strict linearity (which is part of the LCE implementation of the segmented framework) is only an approximation of the relationship between \CO2-induced warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero.
    }
    \label{fig:tcre_bias}
\end{figure*}

\begin{figure*}[t]
    % \includegraphics[]{zncb_updated_framework_performance.jpg}
    \includegraphics[]{figure_5.jpg}
    \caption[Updated segmented framework implementation]{
    Updated implementation of the segmented framework.
    The segmented-NCE implementation represents the relationship between \CO2-induced warming and cumulative \CO2 emissions with a logarithmic fit to \CO2-only experiments based on 1.5\degree C and 2.0\degree C scenarios.
    As a result, it agrees more closely with the results from model runs with interacting climate components than the segmented-LCE implementation with its assumption of strict linearity.}
    \label{fig:other_assessment_framework}
\end{figure*}

\clearpage

\section*{References}
\bibliographystyle{dcu}
\bibliography{bibliography}

% section references (end)

\clearpage

\beginsupplement

\begin{flushleft}

\textbf{Implications of non-linearities between cumulative \CO2 emissions and \CO2-induced warming for assessing the remaining carbon budget: supplementary information}

\input{authorship.tex}

\end{flushleft}

\clearpage

\input{supplementary.tex}

\clearpage

\section*{References}
\bibliography{bibliography}

\end{document}

