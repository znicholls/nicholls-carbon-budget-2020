%!TEX root = main.tex
\section{Background} % (fold)
\label{sec:background}

The SR1.5 approach can be captured by the following equation \cite{IPCC_2018_SR15_Ch_2,rogelj2019estimating}

\begin{equation}
\label{eqn:sronefive_combination_background}
    B_{\mathrm{lim}} = \frac{\Delta T_{\mathrm{lim}} - \Delta T_{\mathrm{hist}} - \Delta T_\mathrm{{nonCO_2}} - \Delta T_{\mathrm{ZEC}}}{\mathrm{TCRE}} - E_{\mathrm{OUP}}
\end{equation}
where $B_\mathrm{lim}$ is the the remaining carbon budget, $\Delta {T_\mathrm{lim}}$ is the peak temperature limit, $\Delta {T_\mathrm{hist}}$ is historical, human-induced warming, $\Delta T_\mathrm{{nonCO_2}}$ is the contribution of non-CO$_2$ climate forcers to warming or cooling at the time when \CO2 emissions reach net zero, $\Delta T_\mathrm{{ZEC}}$ is the zero emissions commitment (ZEC) i.e. the warming or cooling that emerges after CO$_2$ emissions reach net zero, TCRE is the transient climate response to emissions and $E_\mathrm{OUP}$ is a \CO2-equivalent emissions term which represents the impact of processes or feedbacks which are not considered in the other components (we refer to these as `emissions from otherwise unrepresented processes').
All terms are relative to a recent reference period except for $\Delta {T_\mathrm{lim}}$, which is the temperature target relative to a pre-industrial reference period, $\Delta {T_\mathrm{hist}}$, which is the warming between the pre-industrial and recent reference periods, and $\Delta T_\mathrm{{ZEC}}$, which is the warming after \CO2 emissions reach net zero.

\Eref{eqn:sronefive_combination_background} can be written in a more general form as

\begin{equation}
\label{eqn:sronefive_combination_background_clearer}
\eqalign{
    \Delta T_{\mathrm{CO_2}} &= \Delta T_{\mathrm{lim}} - \Delta T_{\mathrm{hist}} - \Delta T_\mathrm{nonCO_2} - \Delta T_{\mathrm{ZEC}}; \cr
    C &= f(\Delta T_{\mathrm{CO_2}}); \cr
    B_{\mathrm{lim}} &= C - E_{\mathrm{OUP}}
}
\end{equation}
where $C$ is cumulative \CO2 emissions at the time \CO2 emissions reach net zero (relative to the reference period), $\Delta T_{\mathrm{CO_2}}$ is \CO2-induced warming from the reference period onwards at the time \CO2 emissions reach net zero and $f$ is a transformation which maps between $\Delta T_{\mathrm{CO_2}}$ and $C$.
As long as $f$ is a one-to-one mapping, a finite remaining carbon budget can be calculated for arbitrary temperature targets.

\Eref{eqn:sronefive_combination_background} and \Eref{eqn:sronefive_combination_background_clearer} are examples of the `segmented framework' because they assess the remaining carbon budget via the combination of a number of separate terms.
The key assumption of the segmented framework is that each contributing term is independent, i.e. there are no temperature contributions arising from interactions between the different components e.g. there is no temperature contribution from \CO2--non-\CO2 feedbacks.

However, the total warming we have seen since industrialisation is the result of non-linear interactions and feedbacks, which are not included in the approximation defined in the segmented framework of \Eref{eqn:sronefive_combination_background_clearer}.
As discussed in \Sref{sec:introduction}, this study is the first attempt to investigate and quantify the implications of using different implementations of the segmented framework.
By using a single model throughout our study, we isolate the impact of methodological choices from individual process quantifications.

% section background (end)

\section{Methods} % (fold)
\label{sec:methods}

In this paper, we follow the same convention as SR1.5 and hence use the term `remaining carbon budget' to refer to the cumulative amount of \CO2 that can be released from a given point in time (e.g. 2018) without ever exceeding a peak warming level like e.g. 1.5\degree C.
It takes all anthropogenic forcers into account e.g. emissions of \CO2, methane, nitrous oxides, and aerosols, multiple feedback processes and any warming which may emerge after \CO2 emissions reach net zero.

This definition explicitly excludes overshoot and budgets calculated in this way are not temperature exceedance budgets \cite{rogelj2016differences}.
Instead, the carbon budgets considered here are compatible with remaining below the given temperature target for all time, subject to non-\CO2 forcers not causing further warming once \CO2-induced warming peaks.
This condition holds for all the SR1.5 scenarios \cite{huppmann2019scenariodata} used in this study.
Accordingly, non-\CO2 effects are included in the assessment, but the analysis does not precisely quantify limits on non-\CO2 emissions.

For estimating the remaining carbon budget we use globally averaged surface air temperature as our metric and provide peak temperature estimates relative to the 1720-1800 reference period.
We choose this reference period as a proxy for pre-industrial following the work of \citeasnoun{hawkins2017estimating}, which suggests that this period may be most appropriate because it had very weak anthropogenic radiative forcing and similar natural radiative forcing to today.
We also assume the same historical warming of 1.02\degree C between 1720-1800 and 2006-2015 in all our calculations.
This 1.02\degree C is comprised of 0.97\degree C between 1850-1900 and 2006-2015 \citeaffixed{IPCC_2018_SR15_Ch_1}{as used in SR1.5,} plus 0.05\degree C between 1720-1800 and 1850-1900 \cite{hawkins2017estimating}.

To match the scenarios and reference period used in SR1.5 exactly, remaining carbon budgets are calculated from 2011 onwards, because the SR1.5 scenarios used throughout this study are harmonised such that all emissions are consistent up until 2010 and vary thereafter.
This leads to a minor and negligible variation of the baseline period surface temperature average (2006-2015) for each scenario.

For all of our calculations, we use the Model for the Assessment of Greenhouse Gas Induced Climate Change, version 6 \citeaffixed{meinshausen2011magiccdescription}{MAGICC6,}.
MAGICC6 is a key scenario assessment tool which is widely used in the IPCC assessment process \cite{IPCC_2014_WGIII,IPCC_2014_SYR} and can be run from Python using Pymagicc \cite{Gieseke_2018}.
Its main components are an upwelling-diffusion ocean, a box-model of the carbon cycle, simplified gas cycles of non-\CO2 species and a permafrost module which follows \citeasnoun{schneider2012estimating}.
We choose MAGICC6 because it incorporates all of the components required to quantify the extent to which the segmented remaining carbon budget assessment framework can approximate non-linear interactions and feedbacks within the climate system.
It is also computationally efficient enough to perform the multiple experiments required by this study.
MAGICC6's representation of the climate system is highly parameterised hence does not include explicit representations of all the interactions within the climate system, particularly at the regional level.
Nonetheless, on a multi-centennial, global-scale, it has been shown to reproduce the temperature response, climate feedbacks and carbon cycle feedbacks from more complex models \cite{meinshausen2011magiccdescription,rogelj2014implications}, such as those from the Third Coupled Model Intercomparison Project \citeaffixed{meehl2007wcrp}{CMIP3,} and Fifth Coupled Model Intercomparison Project \citeaffixed{taylor2012overview}{CMIP5,}.

To examine the sensitivity of our conclusions to the model calibration, we repeat the entire experiment for a range of plausible carbon cycle and ocean responses, described in \citeasnoun{meinshausen2011magiccdescription}.
Each carbon cycle response emulates a different carbon cycle model that participated in the C\textsuperscript{4}MIP experiment \cite{friedlingstein2006climate} while the ocean responses emulate different AOGCM models \cite{meehl2007wcrp}.
We include the MIROC3.2(hires) ocean calibration in the figures but not in our reported ranges because this calibration's zero emissions commitment is a clear outlier.

\subsection{Remaining carbon budget when simulating an interactive climate system} % (fold)
\label{sub:integrated_calculation}

We firstly calculate the remaining carbon budget using model simulations which explicitly consider the interactions and feedbacks within the climate system.
This calculation provides a benchmark, against which we can test implementations of the segmented framework.

We run MAGICC6 including all of the components considered by the SR1.5 framework i.e. all anthropogenic and natural forcings as well as its permafrost module (\Fref{fig:online_runs_overview}).
MAGICC6's \CO2 response component is run in an emissions-driven setup including temperature and fertilisation carbon cycle feedbacks while its non-\CO2 component is run in a concentration-emissions hybrid mode.
In the historical period MAGICC6 has prescribed atmospheric concentrations for non-\CO2 greenhouse gases and prescribed optical thickness for aerosols.
For the projection period, it is driven by anthropogenic emissions of greenhouse gases and aerosols.

We run each of the scenarios assessed in SR1.5 \cite{huppmann2019scenariodata} through this setup.
We then derive cumulative \CO2 emissions compatible with the peak warming in each scenario (Supplementary \Sref{sec:cumulative_co2_emissions_compatible_with_peak_warming_in_model_simulations}).
By performing a linear regression between peak warming and compatible cumulative \CO2 emissions over all scenarios (using the statsmodels Python package \cite{seabold2010statsmodels}), we can determine the remaining carbon budget for any temperature target (\Fref{fig:online_runs_overview}).
When making such a calculation, it should be recognised that the results will likely only hold for ambitious mitigation scenarios i.e. scenarios which reach net zero \CO2 emissions.

% subsection integrated_calculation (end)

\subsection{Implementing the segmented framework} % (fold)
\label{sub:implementing_the_segmented_framework}

When implementing the segmented framework, decisions must be made about how to quantify every component.
In this study, we focus on the impact of differing decisions about the relationship between \CO2-induced warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero.
For all the other components, we use a consistent set of choices throughout the study.
These follow the decisions made in SR1.5 as closely as possible and are described in Supplementary \Sref{sec:choices_made_whilst_implementing_the_segmented_framework}.
The combination of \Sref{sec:methods} and Supplementary \Sref{sec:choices_made_whilst_implementing_the_segmented_framework} covers all the items on the reporting check-list proposed in Supplementary Text 3 of \citeasnoun{rogelj2019estimating}.

The first implementation of the segmented framework follows the SR1.5.
We refer to this implementation as `segmented-LCE' because its key assumption is that, for a given total allowable \CO2-induced warming, there should be a `linear conversion to cumulative \CO2 emissions (LCE)' i.e. the relationship between \CO2-induced warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero is linear.

In contrast, the second implementation uses a `non-linear conversion to cumulative \CO2 emissions (NCE)' hence we refer to it as `segmented-NCE'.
This is a novel implementation which allows the relationship between \CO2-induced warming and cumulative \CO2 emissions at the time of net zero \CO2 emissions to be weakly non-linear whilst still providing a direct link to existing metrics of the climate response to \CO2 emissions.

It should be noted that the two implementations presented here represent only two of a range of possible implementation choices, and that other choices are possible.
Exploring the implications of other choices is an area for future study, beyond the scope of this paper.

The segmented-LCE implementation assumes a linear relationship between \CO2-induced warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero,

\begin{equation}
\label{eqn:sr15_cco2_t}
    \Delta T_{CO_2} = C \times \mathrm{TCRE}
\end{equation}
where $C$ is cumulative \CO2 emissions at the time \CO2 emissions reach net zero (relative to the reference period).

SR1.5's estimate of the TCRE was based on the range from AR5 \cite{IPCC_2013_WGI_Ch_12,IPCC_2013_WGI_TS}, which is itself based on multiple lines of evidence.
The canonical way to derive the TCRE from Earth System Models (ESMs) is described in \citeasnoun{gillett2013constraining} and their model based results (5--95\% TCRE range of $0.8$ -- $2.4$\degree C per TtC) informed the range used in SR1.5 (33-67\% range of $0.8$ -- $2.5$\degree C per TtC).

In order to fit within our `single model' methodology, we do not follow the SR1.5's assumed TCRE range exactly.
We use the TCRE definition and methodology from \citeasnoun{gillett2013constraining}, first introduced by \citeasnoun{matthews2009proportionality} who define the TCRE of a model as the ratio of warming to cumulative \CO2 emissions in a simulation with a prescribed 1\% per year increase in \CO2 (a `1pctCO2' simulation) at the time when \CO2 reaches double its pre-industrial concentration.
Cumulative \CO2 emissions ($C_{2\times}$) and warming ($\Delta T_{2\times}$) at the point in time when \CO2 concentrations double are shown by the crosses in Figures \ref{fig:sr15_budget_calculation_overview}(d) and \ref{fig:sr15_budget_calculation_overview}(e) respectively.
In this study, our default setup has a TCRE of $1.74$\degree C per TtC and the TCRE ranges from $1.2$ -- $2.27$\degree C per TtC over the assessed MAGICC6 calibrations.
These MAGICC6 calibrations emulate various ESMs which, as shown in \citeasnoun{gillett2013constraining}, show a range of slight deviations from a strictly linear relationship between \CO2-induced and warming and cumulative \CO2 emissions.

The segmented-NCE implementation examines the benefits of considering a non-linear relationship between \CO2-induced warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero.
The first step in this implementation is deciding on a specific form for the non-linear relationship between \CO2-induced warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero.
We motivate this form by considering the counteracting processes which lead to a nearly linear relationship between surface warming and cumulative \CO2 emissions.
The key balance is between the weakening of carbon sinks as cumulative anthropogenic \CO2 emissions increase and the near logarithmic relationship between atmospheric \CO2 concentrations and radiative forcing \cite{matthews2009proportionality,macdougall2016tcrereview}.
Following this, we use a logarithmic relationship (\Eref{eqn:this_study_cco2_t}).
For completeness, we recognise that the balance between weakening carbon sinks and logarithmic \CO2 radiative forcing only holds when atmospheric \CO2 concentrations are changing.
When concentrations are approximately constant, an approximately constant TCRE emerges because the increasing sensitivity of warming to radiative forcing \cite{ehlert2017sensitivitytcretooceanmixing} largely balances the decrease in emissions required to keep concentrations constant (due to the weakening carbon sinks).
Even though the exact functional form of these cancellations is uncertain, a logarithmic function is sufficient to examine the impact of deviations from linearity in the relationship between cumulative \CO2 emissions and \CO2-induced warming at the time \CO2 emissions reach net zero.

Having chosen a logarithmic formulation, we seek a function which reduces to strict linearity under certain limits.

\begin{equation}
\label{eqn:this_study_cco2_t}
    \Delta T_{CO_2} = \gamma \Delta T_{2\times} \left[ \frac{\ln(1 + C / \alpha)}{\ln(1 + C_{2\times} / \alpha)} \right]
\end{equation}
Here, as previously, $\Delta T_{\mathrm{CO_2}}$ is \CO2-induced warming from the reference period onwards at the time \CO2 emissions reach net zero and $C$ is cumulative \CO2 emissions at the time \CO2 emissions reach net zero (relative to the reference period).
$\Delta T_{2\times}$ and $C_{2\times}$ are, also as previously, defined as the warming and cumulative \CO2 emissions respectively at the point in time when \CO2 concentrations double in a 1pctCO2 experiment.
The constants $\gamma$ and $\alpha$ allow for pathway dependence and non-linearity in the relationship between \CO2-induced warming and cumulative \CO2 emissions.

To fit the constants $\alpha$ and $\gamma$ in our framework, we run all the SR1.5 scenarios whose \CO2 emissions reach net zero in a \CO2-only setup (without the permafrost module to avoid double counting its contribution).
We then fit \Eref{eqn:this_study_cco2_t} to the warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero from all the simulations (Supplementary \Fref{fig:tcre_diagnosis_methods}).

The reason for this choice becomes more evident if we reconsider \Eref{eqn:sronefive_combination_background_clearer}.
Within \Eref{eqn:sronefive_combination_background_clearer}, the conversion between \CO2-induced warming and cumulative \CO2 emissions need only apply at the time at which \CO2 emissions reach net zero.
This is a subtle, yet important, point.
It means that the conversion between \CO2-induced warming and cumulative \CO2 emissions doesn't need to represent the transient relationship between \CO2-induced warming and cumulative \CO2 emissions over a range of \CO2 emissions rates.
Instead, it must be applicable to a range of cumulative \CO2 emissions but only needs to apply at one \CO2 emissions rate, zero.
In addition, existing literature \cite{krasting2014trajectory} suggests that the relationship between \CO2-induced warming and cumulative \CO2 emissions may be rate-dependent, particularly at low emissions rates.
Hence care must be taken when applying the results from an experiment in which \CO2 emissions do not reach net zero (e.g. the 1pctCO2 experiment) to analyses that do consider net zero scenarios.
Our fitting methodology avoids any potential inconsistencies which might arise from assuming that the transient relationship between cumulative \CO2 emissions and \CO2-induced warming can be directly used to infer the relationship between cumulative \CO2 emissions and \CO2-induced warming at the time \CO2 emissions reach net zero over a range of cumulative \CO2 emissions.

We examine \Eref{eqn:this_study_cco2_t} more closely to explain the choice and effect of the constants in more detail.
Under the limit $\alpha \rightarrow \infty$, \Eref{eqn:this_study_cco2_t} becomes (via a Taylor expansion)

\begin{equation}
\label{eqn:this_study_cco2_t_alpha_to_infty}
    \Delta T_{CO_2} = \gamma \Delta T_{2\times} \left[ \frac{C}{C_{2\times}} \right]
\end{equation}

With $\alpha \rightarrow \infty$, we recover the segmented-LCE implementation's linear relationship between cumulative \CO2 emissions and \CO2-induced warming (\Eref{eqn:sr15_cco2_t}).
By further choosing $\gamma = 1$, we return to the simplest and most common assumption about the relationship between \CO2-induced warming and cumulative \CO2 emissions i.e. $\Delta T_{CO_2} = C \times \mathrm{TCRE}$ with $\mathrm{TCRE} = \Delta T_{2\times} / C_{2\times}$.

Thus, variations in $\gamma$ scale the relationship between \CO2-induced warming and cumulative \CO2 emissions to account for departures from the results of the 1pctCO2 experiment.

In contrast, variations in $\alpha$ control how far \Eref{eqn:this_study_cco2_t} deviates from linearity.
For finite values of $\alpha$, \Eref{eqn:this_study_cco2_t} is non-linear.
If $\alpha$ is positive then the relationship is sub-linear (physically this corresponds to the case where the saturation of CO$_2$ radiative forcing dominates).
On the other hand, negative $\alpha$ leads to a super-linear relationship (weakening of carbon sinks dominating).
It is important to note that the warming at $C=0$ and $C=C_{2\times}$ are independent of the value of $\alpha$.
The curvature between these two points is controlled by $\alpha$, however only variations in $\gamma$ can lead to $\Delta T_{CO_2} (C_{2\times}) \neq T_{2\times}$.

The smaller $\alpha$ becomes, the greater the deviation from linearity.
For example, take $\Delta T_{2\times}$ and $C_{2\times}$ equal to 2.0\degree C and 3 667 Gt\CO2 respectively i.e. a TCRE of $0.54$\degree C per Tt\CO2, equal to the 50\textsuperscript{th} percentile estimate from \citeasnoun{IPCC_2018_SR15_Ch_2}.
For $\alpha = 7334$ Gt\CO2 ($\alpha / C_{2\times} = 2$), cumulative \CO2 emissions compatible with a temperature target of 1.5\degree C are approximately 40 Gt\CO2 bigger and cumulative \CO2 emissions compatible with a temperature target of 2.0\degree C are approximately 170 Gt\CO2 bigger than those inferred from a strictly linear relationship with equivalent TCRE (Supplementary \Sref{sec:impact_of_non_linearity_in_relationship_between_cumulative_co2_emissions_and_co2_induced_warming}, particularly Supplementary \Tref{tab:linear_v_logarithmic_co2_warming}).

% subsection implementing_the_segmented_framework (end)

% section methods (end)
