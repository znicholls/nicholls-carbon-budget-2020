%!TEX root = main.tex

\section{Cumulative \CO2 emissions compatible with peak warming in model simulations} % (fold)
\label{sec:cumulative_co2_emissions_compatible_with_peak_warming_in_model_simulations}

Our simulations include negative \CO2 emissions hence two different cases must be considered when determining the cumulative \CO2 emissions compatible with peak warming (Supplementary \Fref{fig:compatible_peak_warming}).
If maximum cumulative emissions occur after peak warming, i.e. temperatures peak before cumulative emissions peak, then we use maximum cumulative \CO2 emissions because it is almost certain that this level of cumulative \CO2 emissions is consistent with the assessed peak warming.
However, if cumulative emissions peak before temperatures peak, then we use the cumulative emissions at the time of peak warming.
In this case, it would be inappropriate to use the maximum cumulative emissions, as this maximum is unlikely to be compatible with the resultant peak warming.
Put another way, had we not had negative emissions between peak cumulative emissions and peak temperature, the observed peak warming would almost certainly be higher than what was actually realised.
In the limit that peak warming and peak cumulative \CO2 emissions occur at the same time, these two methods converge.

\begin{figure*}[h]
    % \includegraphics[]{zncb_compatible_peak_warming.jpg}
    \includegraphics[]{supplementary_figure_1.jpg}
    \caption[Determining cumulative \CO2 emissions compatible with peak warming from model simulations.]{
    Schematic showing the two different cases which must be considered in order to determine cumulative \CO2 emissions compatible with peak warming from model simulations based on scenarios which also include negative \CO2 emissions.
    }
    \label{fig:compatible_peak_warming}
\end{figure*}

% section cumulative_co2_emissions_compatible_with_peak_warming_in_model_simulations (end)

\clearpage

\section{Choices made whilst implementing the segmented framework} % (fold)
\label{sec:choices_made_whilst_implementing_the_segmented_framework}

When implementing the segmented framework, we have replicated the choices made within the SR1.5 with MAGICC6 as closely as possible.
Where differences do occur, they are clearly stated throughout this section.

In combination with Section 3, we cover every item on the reporting check-list proposed in Supplementary Text 3 of \citeasnoun{rogelj2019estimating}.
We cover each item in as much detail as possible to avoid adding to the confusion that has resulted from differing choices in studies that calculate the remaining carbon budget \cite{rogelj2019estimating}.

In SR1.5, the non-\CO2 warming contribution was assessed using the average of two models: MAGICC6 and FaIRv1.3 \cite{smith2018fairv13}.
Here we only use MAGICC6 to be consistent with our single model approach.
Scenarios are run first with all climate forcers (except for modules such as permafrost which are included elsewhere) and subsequently with \CO2-forcing only, including the historical period (illustrative \CO2 emissions timeseries are given in Figure 2(f)).
The non-\CO2 contribution is then calculated as the difference in global-mean surface temperature between these two experiments (Figure 2(g)), relative to the chosen reference period, at the time \CO2 emissions reach net zero (this point in time is marked with vertical lines in Figure 2(f)).
By repeating this process for a number of scenarios, a regression can be performed between warming at the time \CO2 emissions reach net zero and non-\CO2 warming (Figure 2(h)).

SR1.5 considered a range of literature \cite{matthews2008stabilizing,lowe2009difficult,gillett2011ongoing,frolicher2010reversible} to assess the ZEC.
After considering these studies, which derived the ZEC from `sudden emission cessation' experiments rather than real-world phase-out scenarios, SR1.5 assessed the ZEC to be zero.

Here we perform the ZEC experiments seen in the literature with MAGICC6.
We choose to set our ZEC equal to the median over all the experiments we have performed (Figures 2(i)-(k)).
An important clarification is that in the context of a remaining carbon budget to peak warming, which explicitly excludes overshoot, the ZEC can only be positive or zero.
This is because a drop in global-mean temperatures after \CO2 emissions cease does not change the peak value.

We don't follow the ZECMIP protocol \cite{jones2019zecmip} whilst assessing the ZEC because ZECMIP did not exist at the time of the SR1.5.
Nonetheless, the ZECMIP experiments do not significantly differ from the experiments we have performed and subsequent exploration has shown that following the ZECMIP protocol, rather than the experiments described here, does not affect our conclusions.

SR1.5 did not provide a specific way to calculate the contribution from otherwise unrepresented processes, instead they assessed it from the available literature, most of which considers the contribution of such processes to the carbon budget in relevant mitigation scenarios \citeaffixed{schadel2014circumpolar}{e.g.}.
Examples of processes considered in SR1.5 are the permafrost feedback \cite{lowe2018impact,burke2018co2,macdougall2015sensitivity,schneider2015observationpermafrost,friedlingstein2014uncertainties}, additional methane releases from wetlands \cite{comyn2018carbon} and aerosol-biogeochemistry changes \cite{mahowald2017aerosol}.

Here we use MAGICC6's permafrost module to estimate the contribution of otherwise unrepresented processes.
We set our estimate of the contribution of unrepresented processes equal to the median cumulative permafrost emissions in 2100, over all the scenarios assessed in SR1.5 (blue cross in Figure 2(l)).
We choose 2100 as our year of interest for consistency with SR1.5.

\citeasnoun{lowe2018impact} suggest that considering permafrost only is a reasonable first-order estimate because other effects largely cancel.
However, MAGICC6 already captures some of the negative feedbacks considered by \citeasnoun{lowe2018impact} (e.g. the climate CH\textsubscript{4} lifetime feedback) so considering permafrost only is likely to be an underestimate of the true contribution.


% section choices_made_whilst_implementing_the_segmented_framework (end)

\section{Impact of non-linearity in relationship between cumulative \CO2 emissions and \CO2-induced warming} % (fold)
\label{sec:impact_of_non_linearity_in_relationship_between_cumulative_co2_emissions_and_co2_induced_warming}

To explore the impact of non-linearity within our framework, we start with Equations 3 and 4 from the main text.
Next, we re-write these two equations so that cumulative \CO2 emissions, $C$, is the subject.

\begin{equation}
\label{eqn:sr15_cco2_t_inverse}
    C = \frac{\Delta T_{CO_2}}{\mathrm{TCRE}}
\end{equation}

\begin{equation}
\label{eqn:this_study_cco2_t_inverse}
    C = \alpha \left[\exp \left(\frac{\Delta T_{CO_2}}{\gamma \Delta T_{2\times}}\ln(1 + \frac{C_{2\times}}{\alpha}) \right) - 1 \right]
\end{equation}

If we are considering cumulative \CO2 emissions and \CO2-induced warming relative to pre-industrial, then the difference between the segmented-LCE and segmented-NCE implementations is simply the difference between Equations \ref{eqn:sr15_cco2_t_inverse} and \ref{eqn:this_study_cco2_t_inverse}.

However, if we wish to consider cumulative \CO2 emissions and \CO2-induced warming relative to a more recent reference period then things are slightly more complex.
We cannot simply take the difference of Equations \ref{eqn:sr15_cco2_t_inverse} and \ref{eqn:this_study_cco2_t_inverse}.
Instead, we must first make Equations \ref{eqn:sr15_cco2_t_inverse} and \ref{eqn:this_study_cco2_t_inverse} relative to the reference period of interest.

For a given historical \CO2-induced warming estimate ($\Delta T_{\mathrm{hist-CO_2}}$), the relationship between cumulative \CO2 emissions relative to the reference period of interest ($C_r$) and \CO2-induced warming relative to the reference period of interest ($\Delta T_r$) is given by:

\begin{equation}
\label{eqn:rebasing_cco2_t}
    C_r = C(\Delta T_r + \Delta T_{\mathrm{hist-CO_2}}) - C(\Delta T_{\mathrm{hist-CO_2}})
\end{equation}

In other words, we calculate the compatible cumulative \CO2 emissions relative to pre-industrial and then remove the cumulative \CO2 emissions which have lead to the historical \CO2-induced warming, leaving only cumulative \CO2 emissions from the reference period onwards.

It is worth noting that Equations \ref{eqn:sr15_cco2_t_inverse} and \ref{eqn:this_study_cco2_t_inverse} need not always be relative to pre-industrial.
However, if \Eref{eqn:this_study_cco2_t_inverse} is not relative to pre-industrial, then some care must be taken as the constants $\Delta T_{2\times}$ and $C_{2\times}$ will no longer directly relate to the results of a 1pctCO2 experiment, which is by definition always relative to pre-industrial.

We combine Equations \ref{eqn:sr15_cco2_t_inverse}, \ref{eqn:this_study_cco2_t_inverse} and \ref{eqn:rebasing_cco2_t} to derive the values given in \Tref{tab:linear_v_logarithmic_co2_warming}.
The impact of applying Equation \ref{eqn:rebasing_cco2_t} and the differences between the linear and non-linear relationship between cumulative \CO2 emissions and \CO2-induced warming are shown in \Fref{fig:linear_v_logarithmic_co2_t}.

\input{linear_v_logarithmic_table}

\begin{figure*}[h]
    % \includegraphics[width=\textwidth]{linear_v_logarithmic_equations.pdf}
    \includegraphics[width=\textwidth]{supplementary_figure_2.pdf}
    \caption[Influence of $\alpha$ on the relationship between cumulative \CO2 emissions and \CO2-induced warming.]{
    Influence of $\alpha$ on the relationship between cumulative \CO2 emissions and \CO2-induced warming.
    Here we compare the strictly linear relationship between cumulative \CO2 emissions and \CO2-induced warming (solid line) and our non-linear relationship between cumulative \CO2 emissions and \CO2-induced warming (dashed lines) for a range of values of the ratio of $\alpha$ to $C_{2\times}$.
    (a) Relationship between cumulative \CO2 emissions and \CO2-induced warming relative to pre-industrial; (b) Relationship between cumulative \CO2 emissions and \CO2-induced warming relative to a recent reference period; (c) Difference between compatible cumulative \CO2 emissions relative to a recent reference period derived from the non-linear and linear assumptions.
    For positive (negative) values of $\alpha / C_{2\times}$, the non-linear relationship results in larger (smaller) compatible cumulative \CO2 emissions.
    Values shown here are calculated with $C_{2\times} = 3667$ Gt\CO2, $T_{2\times} = 2$\degree C, $\gamma = 1$ and a historical \CO2-induced warming estimate of $1.0$\degree C.}
    \label{fig:linear_v_logarithmic_co2_t}
\end{figure*}

% section impact_of_non_linearity_in_relationship_between_cumulative_co2_emissions_and_co2_induced_warming (end)

\clearpage

\begin{figure*}[t]
    % \includegraphics[]{zncb_tcre_diagnosis_methods_update.jpg}
    \includegraphics[]{supplementary_figure_3.jpg}
    \caption[Calculating the relationship between \CO2-induced warming and cumulative \CO2 emissions from scenario based experiments]{Calculating the relationship between \CO2-induced warming and cumulative \CO2 emissions from scenario based experiments. The results of the experiments are used to perform a fit between cumulative \CO2 emissions and \CO2-induced temperature rise.
    }
    \label{fig:tcre_diagnosis_methods}
\end{figure*}

\clearpage

\input{fit_metrics_table.tex}

\clearpage

\section{Reporting results within the remaining carbon budget assessment framework} % (fold)
\label{sec:reporting_results_within_the_remaining_carbon_budget_assessment_framework}

Deriving functions which allow the remaining carbon budget to be calculated for arbitrary temperature targets ($\Delta T_{\mathrm{lim}}$) following the segmented framework is more complex than Equation 2 suggests.
We would argue that this is because the segmented framework isn't designed for deriving continuous functions.
Rather, it is designed to provide a way to breakdown the influence of different climatic components on remaining carbon budget estimates for discrete temperature targets.

A function of the form of Equation 2 can be written as a function of $\Delta T_{\mathrm{lim}}$ if, and only if, each of the components in Equation 2 can be written as a function of $\Delta T_{\mathrm{lim}}$.
This is not always the case.
For example one of the components might only be evaluated at a few discrete values of $\Delta T_{\mathrm{lim}}$ and there may be no analytical relationship between this component's contribution and $\Delta T_{\mathrm{lim}}$.

Even in the simplest case, writing Equation 2 as a function of $\Delta T_{\mathrm{lim}}$ only may completely obscure the contribution of each component, defeating the point of the SR1.5 framework.
Hence we suggest instead comparing the contributions of each climate component at discrete values of $\Delta T_{\mathrm{lim}}$ (using e.g. a table of the form of Table 2).
This practice is likely to provide much more insight into the drivers of differences between remaining carbon budget estimates than attempting to force everything into a single function of $\Delta T_{\mathrm{lim}}$.

To illustrate why deriving single functions of $\Delta T_{\mathrm{lim}}$ is difficult, we show the steps required to do so for the segmented-LCE implementation (the steps are identical for the segmented-NCE implementation except the contribution of \CO2 must be altered).
We start with Equation 2.
For the SR1.5 framework, $f$ is simply given by Equation \ref{eqn:sr15_cco2_t_inverse}.
In contrast with $f$, the non-\CO2 warming contribution is relatively more complex.
The complexity is because we have calculated the non-\CO2 warming as a linear function of warming at the time \CO2 emissions reach net zero relative to the chosen reference period ($\Delta T_\mathrm{nz}$).
In other words, we have $\Delta T_\mathrm{{nonCO_2}} = m\Delta T_\mathrm{nz} + \phi$ where $m$ and $\phi$ are constants and $\Delta T_\mathrm{nz}$ depends on the estimate of historical warming and the ZEC.
Putting these together we have,

\begin{equation}
\label{eqn:delta_t_nz}
\eqalign{
    \Delta T_\mathrm{nz} &= \Delta T_{\mathrm{lim}} - \Delta T_{\mathrm{hist}} - \Delta T_{\mathrm{ZEC}}; \cr
    \Delta T_\mathrm{{nonCO_2}} &= m(\Delta T_{\mathrm{lim}} - \Delta T_{\mathrm{hist}} - \Delta T_{\mathrm{ZEC}}) + \phi
}
\end{equation}

Combining Equations 1, \ref{eqn:sr15_cco2_t_inverse} and \ref{eqn:delta_t_nz}, we have

\begin{equation}
\label{eqn:all_combined}
\eqalign{
    B_{\mathrm{lim}} = &\frac{\Delta T_{\mathrm{lim}} - \Delta T_{\mathrm{hist}} - (m(\Delta T_{\mathrm{lim}} - \Delta T_{\mathrm{hist}} - \Delta T_{\mathrm{ZEC}}) + \phi) - \Delta T_{\mathrm{ZEC}}}{\mathrm{TCRE}} \cr &- E_{\mathrm{OUP}}
}
\end{equation}

Within the segmented-LCE implementation, we can now calculate the budget for arbitrary $\Delta T_{\mathrm{lim}}$.
However, \Eref{eqn:all_combined} doesn't make each climate component's contributions explicitly clear.
The contribution from each component becomes even less clear if we re-write \Eref{eqn:all_combined} in the more familiar form of a linear equation

\begin{equation}
\label{eqn:all_combined_simplified}
\eqalign{
    B_{\mathrm{lim}} &= m^* \Delta T_{\mathrm{lim}} + c^*; \cr
    m^* &= \frac{1 - m}{\mathrm{TCRE}}; \cr
    c^* &= \frac{-(\Delta T_{\mathrm{hist}} + \Delta T_{\mathrm{ZEC}})(1 - m) - \phi}{\mathrm{TCRE}} - E_{\mathrm{OUP}}
}
\end{equation}

To summarise and reiterate.
It is not always possible to write $B_{\mathrm{lim}} = h(\Delta T_{\mathrm{lim}})$, where $h$ is some analytical function.
Even in the simple cases where $h$ can be found, we feel that the loss of comparibility which occurs goes against the spirit of the independence assumptions in the segmented framework.
Hence we would instead recommend reporting the contributions from the different components independently for relevant peak warming targets.
Like \citeasnoun{rogelj2019estimating}, we also feel that such a practice will lead to much simpler comparison of remaining carbon budget estimates and hence far less confusion about this key topic.

% section reporting_results_within_the_remaining_carbon_budget_assessment_framework (end)

\clearpage

\begin{figure*}[h]
    % \includegraphics[width=\textwidth]{zncb_tcre_diagnosis_bias_all_carbon_cycle.jpg}
    \includegraphics[width=\textwidth]{supplementary_figure_4.jpg}
    \caption[TCRE bias with other carbon cycles]{
    Non-linearity in the relationship between \CO2-induced warming and cumulative \CO2 emissions, for each of MAGICC6's available carbon cycle tunings.
    The assumption of strict linearity does not always reflect the relationship between warming and cumulative \CO2 emissions seen in a range of experiments in which \CO2 emissions reach net zero.
    However, the size and direction of this discrepancy varies as a function of the dynamics of the carbon cycle.
    The model to which MAGICC6's carbon cycle has been tuned is shown in the top-left hand corner of each panel.}
    \label{fig:tcre_other_carbon_cycle}
\end{figure*}

\begin{figure*}[h]
    % \includegraphics[width=\textwidth]{zncb_tcre_diagnosis_bias_ocean.jpg}
    \includegraphics[width=\textwidth]{supplementary_figure_5.jpg}
    \caption[TCRE bias with other oceans]{
    Non-linearity in the relationship between \CO2-induced warming and cumulative \CO2 emissions, for each of MAGICC6's available ocean tunings.
    The assumption of strict linearity does not always reflect the relationship between warming and cumulative \CO2 emissions seen in a range of experiments in which \CO2 emissions reach net zero.
    However, the size and direction of this discrepancy varies as a function of the dynamics of the ocean.
    The model to which MAGICC6's ocean has been tuned is shown in the top-left hand corner of each panel.}
    \label{fig:tcre_other_ocean}
\end{figure*}

\begin{figure*}[h]
    % \includegraphics[width=0.95\textwidth]{zncb_updated_framework_performance_all_carbon_cycles.jpg}
    \includegraphics[width=0.95\textwidth]{supplementary_figure_6.jpg}
    \caption[Updated segmented framework implementation for all carbon cycle tunings]{
    Impact of updating the implementation of the segmented framework for each of MAGICC6's available carbon cycle tunings.
    In all cases, using the updated segmented-NCE implementation proposed in this study, rather than the SR1.5-style segmented-LCE implementation, results in an improvement or little change in agreement between the segmented framework and results from simulations with interacting climate components.
    }
    \label{fig:updated_framework_other_carbon_cycle}
\end{figure*}

\begin{figure*}[h]
    % \includegraphics[height=0.8\textheight]{zncb_updated_framework_performance_all_oceans.jpg}
    \includegraphics[height=0.8\textheight]{supplementary_figure_7.jpg}
    \caption[Updated segmented framework implementation for all ocean tunings]{
    Impact of updating the implementation of the segmented framework for each of MAGICC6's available ocean tunings.
    In all cases, using the updated segmented-NCE implementation proposed in this study, rather than the SR1.5-style segmented-LCE implementation, results in an improvement or little change in agreement between the segmented framework and results from simulations with interacting climate components.
    }
    \label{fig:updated_framework_other_ocean}
\end{figure*}

\input{fitting_coefficients_table.tex}

% subsection limitations (end)
