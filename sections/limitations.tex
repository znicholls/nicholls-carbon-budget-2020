%!TEX root = main.tex
\section{Limitations} % (fold)
\label{sub:limitations}

The results presented here come with a number of limitations that call for future research beyond this study.
To start with, we have analysed implementations of the segmented framework in only one way.
While our analysis is able to identify one key area for improvement, other approaches may identify other weaknesses, for example those that may currently be hidden because of compensating errors.

Secondly, determining $\alpha$ and $\gamma$ using exactly the same methodology as this study cannot practically be done with ESMs because of the large number of simulations required.
The segmented framework allows problems like this to be overcome because it combines multiple lines of evidence.
This is most obvious in the inclusion of otherwise unrepresented processes, which ensures that the processes which are currently not assessed by ESMs are nonetheless considered.
We feel that the segmented-NCE implementation follows exactly the same logic.
The starting point could still be strict linearity ($\alpha \rightarrow \infty$, $\gamma=1$), based on the TCRE from ESMs.
This could be extended with estimates of $\alpha$ from ESMs' existing 1pctCO2 simulations.
Then other lines of evidence, such as well-calibrated emulators or observations (although disentangling estimates of the different components e.g. the contribution of \CO2 separate from non-\CO2 factors would certainly be a challenge), can further inform the magnitude of the non-linearity and scenario dependence in the relationship between \CO2-induced warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero.
\Eref{eqn:this_study_cco2_t} can then be used to combine multiple lines of evidence, utilising their respective strengths and ensuring that the impact of non-linearity and scenario dependence in the relationship between \CO2-induced warming and cumulative \CO2 emissions is included in remaining carbon budget assessment.

Thirdly, we have not considered how uncertainty in the overall remaining carbon budget can be quantified.
As acknowledged in SR1.5 \cite{IPCC_2018_SR15_Ch_2}, the segmented framework provides no way to formally combine all the relevant uncertainties into an overall uncertainty.
This is because the components and their uncertainties are assessed and combined independently.
In contrast, model simulations with interacting climate components could be extended to quantify uncertainty by using a probabilistic parameter set of the kind developed by \citeasnoun{meinshausen2009greenhouse}.
By sampling a range of plausible climate responses, a probability density function for the carbon budget as a function of peak warming could be determined.
We do not attempt such an assessment here as there are no formally derived, up-to-date distributions available and developing one is beyond the scope of this paper.
Nonetheless, in the area of uncertainties, model simulations with interacting climate components offer a more obvious way forward than the segmented framework.

Fourthly, non-\CO2 assessment and related uncertainties have not been discussed in any detail.
Problems are immediately evident in the low R-squared values seen in the regression between non-\CO2 warming and warming at the time of net zero \CO2 emissions (Supplementary \Sref{sec:choices_made_whilst_implementing_the_segmented_framework} and Supplementary \Tref{tab:fitting_metrics}).
Reconsidering how the non-\CO2 contribution is assessed within the segmented framework is an obvious area for further examination.

Finally, it is still not clear how land-use change \CO2 emissions should be included.
As \citeasnoun{simmons2016assessing} note, there is little research into whether the climate response to land-use change \CO2 emissions might differ from the climate response to fossil \CO2 emissions (as one directly changes the size of the global land carbon pool whilst the other doesn't).
The lack of a clear protocol about whether diagnosis experiments should include land-use change emissions or not is a problem for clarity, consistency and comparability.
