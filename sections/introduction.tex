%!TEX root = main.tex
\section{Introduction} % (fold)
\label{sec:introduction}

Carbon budgets relate cumulative emissions of carbon dioxide (\CO2) to global-mean temperature change.
The carbon budget concept gained widespread attention in 2009 \cite{allen2009warming,matthews2009proportionality,meinshausen2009greenhouse,zickfeld2009setting} and ever since a range of scientific literature has assessed and quantified it \citeaffixed{millar2017emission,tokarska2018cumulative,goodwin2018pathways}{recently e.g.}.

Though conceptually simple, the details of a carbon budget derivation are complex \cite{rogelj2016differences}.
As a metric, the carbon budget combines multiple characteristics of the Earth system's response to anthropogenic emissions into a single number: how much \CO2 can be released into the atmosphere without exceeding a given warming threshold.
Accordingly, it is sensitive to a number of factors including estimates of the climate system's response to \CO2 emissions \cite{raupach2013exponential,IPCC_2013_WGI_Ch_12,gillett2013constraining} and assumptions about the impact of non-\CO2 climate forcers \cite{mengis2018carbon,simmons2016assessing}.
While carbon budgets are widely used, recent re-assessments have led to criticism of the concept, in particular for being subject to large uncertainties \cite{peters2018beyond}.

The IPCC's Special Report on Global Warming of 1.5\degree C (SR1.5) introduced a new framework to assess the relationship between warming and cumulative \CO2 emissions \cite{IPCC_2018_SR15_Ch_2} that was later extended and formalized in \citeasnoun{rogelj2019estimating}.
The new, `segmented' framework (\Sref{sec:background}) separately quantifies the contributions of different climate system components, considering historical warming, \CO2-induced warming, warming due to non-\CO2 climate drivers, the zero emissions commitment and the impact of otherwise unrepresented processes.
These separate assessments are then combined to derive the overall relationship between cumulative \CO2 emissions and warming.

The segmented framework has two clear advantages.
Firstly, it ensures that assumptions about the importance of different components of the climate system are explicit.
Secondly, the assessments of each component are assumed to be independent, hence they can be sourced from specialist research communities and multiple lines of evidence.

The framework's simplicity may also come with disadvantages.
The climate system includes feedbacks and interactions between its components, which are explicitly not included in the segmented framework.

In the SR1.5's implementation of the framework, a strictly linear relationship between cumulative \CO2 emissions and \CO2-induced warming at the time of net zero \CO2 emissions was assumed.
However, the linear relationship between cumulative \CO2 emissions and \CO2-induced warming is a first-order approximation \cite{macdougall2016tcrereview}.
For Earth System Models (ESMs), small deviations from linearity can be seen in \citeasnoun{gillett2013constraining}.
The linear approximation overestimates warming for most models, particularly BNU-ESM, CanESM2 and HadGEM2-ES (note that the y-axis in Figure 1(c) of \citeasnoun{gillett2013constraining} is relative: for cumulative emissions above 1,000 GtC, residuals as small as 0.1 are absolute deviations of 0.1\degree C or more).
For some Earth System Models of Intermediate Complexity (EMICs), the deviations from linearity are yet more pronounced \citeaffixed{macdougall2016tcrereview}{see e.g.}.
Given that errors of this size can change 1.5\degree C remaining carbon budget estimates by 200 Gt\CO2 ($\sim 20$\%) \cite{IPCC_2018_SR15_Ch_2}, a linear approximation may not be sufficiently precise for estimating our rapidly dwindling remaining carbon budget.
The assumption of strict linearity also implies that the transient climate response to emissions (TCRE) is independent of the rate of emissions.
In contrast, previous studies detect a small dependence on the rate of \CO2 emissions, particularly at low emissions rates \cite{krasting2014trajectory}.

Hence we also examine the results of implementing the segmented framework without the assumption of strict linearity.
Our updated implementation allows for a non-linear relationship between cumulative \CO2 emissions and \CO2-induced warming whilst also providing a direct link to existing methods and metrics.

The study begins by discussing the theoretical background of the segmented framework.
We then describe the methods used to quantify our key question i.e. the impact of implementing the segmented framework under the assumption of a strictly linear relationship between cumulative \CO2 emissions and \CO2-induced warming.

We find that the SR1.5's assumption of strict linearity between cumulative \CO2 emissions and \CO2-induced warming limits the ability to replicate model simulations which include climate feedbacks and interactions.
However this discrepancy can be greatly reduced if the segmented framework is implemented without this assumption.

The paper is structured in the following way.
Firstly, we calculate the remaining carbon budget from model simulations which include interactions between the climate system's components (\Fref{fig:online_runs_overview}).
This provides a benchmark against which we can test the extent to which different implementations of the segmented framework approximate the non-linear interactions and feedbacks within the climate system.
Secondly, we implement the segmented framework following the assumption of strict linearity between \CO2 emissions and \CO2-induced warming (\Fref{fig:sr15_budget_calculation_overview}) and compare it to our model simulations which include interactions between the climate system's components (\Fref{fig:online_offline_budget_temperature_cumulative_co2}).
This is, to our knowledge, the first time the performance of implementations of the segmented framework has been investigated and quantified.
Thirdly, we propose a new (weakly non-linear) parameterisation to capture the relationship between cumulative \CO2 emissions and \CO2-induced warming (\Fref{fig:tcre_bias}).
Fourthly, we demonstrate that implementing the segmented framework with our updated parameterisation results in better agreement with the results from model simulations which include interactions between the climate system's components (\Fref{fig:other_assessment_framework}).

We conclude that the segmented framework's independence assumptions do not introduce any significant error in and of themselves.
However, the choices made whilst implementing the framework are important and can introduce unintended inconsistencies.
In particular, the non-linearity in the relationship between cumulative \CO2 emissions and \CO2-induced warming alters remaining carbon budget estimates by around 10\%.
We recommend that implementations of the segmented framework consider our update in order to capture this effect.

% section introduction (end)
