%!TEX root = main.tex
\section{Results and Discussion} % (fold)
\label{sec:results}

We find that the segmented-LCE implementation in our single-model experiment yields notably different results from the actual model simulations (Figure \ref{fig:online_offline_budget_temperature_cumulative_co2}).
For a target peak temperature of 1.5\degree C above pre-industrial \citeaffixed{hawkins2017estimating}{represented by the period 1720-1800,} the difference in remaining carbon budget estimates is approximately \onlineofflinesronefivestyleonefivebudgetgtcotwodifference{} (\onlineofflinesronefivestyleonefivebudgetpercentagedifference).
This difference is slightly larger than the recent emissions uncertainty in Table 2.2 of SR1.5 (20 Gt\CO2).
For a target peak temperature of 2.0\degree C above pre-industrial the difference grows and the methods disagree by \onlineofflinesronefivestyletwobudgetgtcotwodifference{} (\onlineofflinesronefivestyletwobudgetpercentagedifference) (Table \ref{tab:carbon_budget_results}).
This difference is a similar size to the non-\CO2 scenario variation and historical temperature uncertainties presented in Table 2.2 of SR1.5 (250 Gt\CO2).

Allowing for non-linearity in the relationship between cumulative \CO2 emissions and \CO2-induced warming, as in the segmented-NCE implementation, significantly reduces this difference (Figure \ref{fig:other_assessment_framework}).
The difference between remaining carbon budget estimates based on the segmented-NCE implementation and estimates based on model simulations with interacting climate components is around \onlineofflinethisstudyonefivebudgetgtcotwodifference{} for 1.5\degree C of warming and only \onlineofflinethisstudytwobudgetgtcotwodifference{} (\onlineofflinethisstudytwobudgetpercentagedifference) for 2.0\degree C of warming.

The non-zero y-intercept of the solid lines in \Fref{fig:other_assessment_framework} implies that, under the assumptions of the segmented framework, if we were to stop emitting immediately, we would still see a temperature rise of approximately $0.2$\degree C.
This is because of the non-\CO2 contribution, ZEC and emissions from otherwise unrepresented processes, all of which are assessed to be non-zero even if \CO2 emissions cease immediately.
However, given that the components of the segmented framework have not been assessed under such immediate cessation scenarios, it should also not be used to make conclusions about `locked-in' warming.
If the segmented framework were to be used to assess warming for such rapid emissions reductions then the assessment of these components should be reconsidered.

Compared to the segmented-LCE implementation, the segmented-NCE implementation better captures the relationship between cumulative \CO2 emissions and \CO2-induced warming.
The improvement becomes particularly pronounced when we are interested in warming and cumulative emissions from a recent reference period, rather than pre-industrial (Figures \ref{fig:tcre_bias}(a) and \ref{fig:tcre_bias}(b)).

There are two causes of this improved representation.
The first is that, as previously discussed, the relationship between cumulative \CO2 emissions and \CO2-induced warming is weakly non-linear.
This is the case in the explored MAGICC6 calibrations as well as the models they are emulating (see Fig. 1 of \citeasnoun{gillett2013constraining} and Fig. 1c of \citeasnoun{macdougall2016tcrereview}).
This is reflected in the results of the 1pctCO2 experiment (\Fref{fig:tcre_bias}, specifically the difference between the orange dashed line and the solid gray line in \Fref{fig:tcre_bias}(c)).
Fitting \Eref{eqn:this_study_cco2_t} to the transient relationship between warming and cumulative \CO2 emissions from the 1pctCO2 experiment (with $\gamma=1$ to ensure that our fit passes through the TCRE), we find $\alpha=$ \offlinetcrealpha{} (red dashed lines in \Fref{fig:tcre_bias}).
For our default setup, this results in $\alpha / C_{2\times} \sim 2$ (given $C_{2\times} = 4168$ Gt\CO2, Supplementary \Tref{tab:fitting_coefficients}) and hence deviations from linearity of order $100$ Gt\CO2 (Supplementary \Tref{tab:linear_v_logarithmic_co2_warming}).

The second reason for our improved representation of the relationship between cumulative \CO2 emissions and \CO2-induced warming is that we fit to the warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero from scenario based experiments.
This change in fitting dataset allows us to capture the difference between two subtly different relationships: 1) the transient relationship between \CO2 warming and cumulative \CO2 emissions and 2) the relationship between \CO2 warming and cumulative \CO2 emissions at the time \CO2 emissions reach net zero.
This difference is highlighted in \Fref{fig:tcre_bias}, specifically the difference between the red dashed line and the blue dashed line in \Fref{fig:tcre_bias}(c).
As discussed in \Sref{sub:implementing_the_segmented_framework}, this difference may arise because we focus on warming at the time of net zero \CO2 emissions and the climate's response to \CO2 emissions depends on the \CO2 emissions rate itself \cite{krasting2014trajectory}.
We find a fitted value of $\gamma=$ \scenariotcregamma{} i.e. we must scale the results of the 1pctCO2 experiment down by 6\% in order to match the results from 1.5\degree C and 2.0\degree C scenarios.

We find that there is a discrepancy between the peak warming -- cumulative \CO2 emissions relationship diagnosed from the segmented-LCE implementation and the relationship derived from the segmented-NCE implementation for all model calibrations (Supplementary Figures \ref{fig:tcre_other_carbon_cycle} and \ref{fig:tcre_other_ocean}).
However, the discrepancy is not uniform, with both the linearity and scaling of results from the 1pctCO2 experiment varying amongst the different model calibrations (Supplementary Table \ref{tab:fitting_coefficients}).
We find a weak correlation where calibrations whose ocean heat uptake efficiency declines more (less) strongly in response to warming have more (less) linear responses i.e. larger (smaller) $\alpha$.
Although examining the physical drivers of non-linearity in detail is beyond the scope of this paper, at first consideration this appears to agree with our previous reasoning.
A decline in ocean heat uptake efficiency will increase the sensitivity of warming to radiative forcing, leading to greater cancellation with the approximately logarithmic \CO2 radiative forcing and hence a more linear response.

In most cases, we find that using the segmented-NCE implementation, rather than the segmented-LCE implementation, results in noticeably smaller deviations from the results of model simulations with interacting climate components (Supplementary Figures \ref{fig:updated_framework_other_carbon_cycle} and \ref{fig:updated_framework_other_ocean}).
The exception is for peak temperature limits above 2.0\degree C relative to pre-industrial.
Given that our study focuses on ambitious mitigation scenarios, we have low confidence that either of the segmented implementations really represents the dynamics of the system at higher warming targets where feedbacks become more important.
In the interests of scope, we leave further exploration for future study.

Given the improved performance of the segmented-NCE implementation across a wide range of carbon cycle and ocean responses, we feel that there is a clear need to allow for a non-linear relationship between cumulative \CO2 emissions and \CO2-induced warming within remaining carbon budget assessment.
Under the the segmented-LCE implementation it is not possible to consider a non-linear relationship and hence its effect might be missed.

Given the range of possible implementations of the segmented framework, we suggest a small adjustment to the reporting suggested by \citeasnoun{rogelj2019estimating}.
We suggest reporting remaining carbon budget assessments in the style of \Tref{tab:carbon_budget_components_breakdown}, where the implied TCRE can itself vary with the peak warming target.
This provides greater flexibility for remaining carbon budget assessment studies without reducing the ability to compare their conclusions.
In contrast, we do not recommend attempting to summarise results within a single equation because this may not always be possible and can quickly obscure the contributions of the different climate components (Supplementary \Sref{sec:reporting_results_within_the_remaining_carbon_budget_assessment_framework}).

At this point the strengths of the segmented framework are clear.
In particular, it facilitates comparison of differences between remaining carbon budget estimates (e.g. \Tref{tab:carbon_budget_components_breakdown} makes clear that the only difference between the segmented-LCE implementation and the segmented-NCE implementation is the treatment of \CO2).
On the other hand, our linear regression based on model simulations with interacting climate components alone simply results in the relationship $\Delta {T_\mathrm{lim}} = 0.51$\degree C / Tt\CO2 $\times C + 0.22$\degree C (R-squared $= 0.98$, Supplementary \Tref{tab:fitting_metrics}).
From this, no information about the breakdown of contributions from each climatic component can be inferred, hindering easy diagnosis of differences from other estimates.

% section results (end)
