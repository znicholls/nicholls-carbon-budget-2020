# Nicholls et al. ERL 2020

Figures: [![figure pipeline status](https://gitlab.com/znicholls/nicholls-carbon-budget-2020/badges/master/pipeline.svg)](https://gitlab.com/znicholls/nicholls-carbon-budget-2020/commits/master)

DOI: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3726578.svg)](https://doi.org/10.5281/zenodo.3726578)

## Citation

If this repository is of use to you, please cite

> Z R J Nicholls *et al* 2020 *Environ. Res. Lett.* **15** 074017 https://doi.org/10.1088/1748-9326/ab83af

BibTex

```
@article{nicholls_erl_2020,
	author={Zebedee Nicholls and Robert Gieseke and Jared Lewis and Alexander Nauels and Malte Meinshausen},
	title={Implications of non-linearities between cumulative CO2 emissions and CO2 -induced warming for assessing the remaining carbon budget},
	journal={Environmental Research Letters},
	url={http://iopscience.iop.org/10.1088/1748-9326/ab83af},
	year={2020},
	abstract={To determine the remaining carbon budget, a new framework was introduced in the Intergovernmental Panel on Climate Change's Special Report on Global Warming of 1.5° C (SR1.5). We refer to this as a `segmented' framework because it considers the various components of the carbon budget derivation independently from one another. Whilst implementing this segmented framework, in SR1.5 the assumption was that there is a strictly linear relationship between cumulative CO2 emissions and CO2-induced warming i.e. the TCRE is constant and can be applied to a range of emissions scenarios. Here we test whether such an approach is able to replicate results from model simulations that take the climate system's internal feedbacks and non-linearities into account. Within our modelling framework, following the SR1.5's choices leads to smaller carbon budgets than using simulations with interacting climate components. For 1.5° C and 2° C warming targets, the differences are 50 GtCO2 (or 10%) and 260 GtCO2 (or 17%), respectively. However, by relaxing the assumption of strict linearity, we find that this difference can be reduced to around 0 GtCO2 for 1.5° C of warming and 80 GtCO2 (or 5%) for 2.0° C of warming (for middle of the range estimates of the carbon cycle and warming response to anthropogenic emissions). We propose an updated implementation of the segmented framework that allows for the consideration of non-linearities between cumulative CO2 emissions and CO2-induced warming.}
}
```


## Generating the Results

With the requirements above (on some systems you may also need to install `gcc` (Mac OSX) or `gcc_linux-64` (unix) via conda), you need to do two steps:

- `cd nicholls-carbon-budget-2020; git lfs install; git pull`
- `make -B all` (or `make -B all 2>&1 | tee crunching.log` if you want logs on screen and to a file)
    - if you want to do a test run, do `export CI=True;make -B all`

The outputs from `make` should guide you through the required steps to create and activate the correct conda environment.

## Thank you

- [wtbarnes](https://github.com/wtbarnes) for their efforts with https://github.com/wtbarnes/astro_paper_template, this provided the basis for the automation of this repository
- [jhamrick](https://github.com/jhamrick) for their efforts with [nbflow](https://github.com/jhamrick/nbflow)
