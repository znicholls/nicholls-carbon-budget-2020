import datetime


import numpy as np
import pandas as pd


from openscm.scmdataframe import ScmDataFrame
from openscm.units import _unit_registry as unit_registry


CUMULATIVE_EMISSIONS_REF_YEAR = 2011
CUMULATIVE_EMISSIONS_REF_PERIOD_ID_STRING = "(rel. to 2011)"
REF_PERIOD_START = 2006
REF_PERIOD_END = 2015
REF_PERIOD_ID_STRING = "(rel. to 2006-2015)"

SCENARIO_ASSESSMENT_IDX = [
    "time",
    "model",
    "scenario",
    "climate_model",
    "carbon_cycle_tuning",
]


def _scenario_assessment_pivot(in_iamdf):
    pivot_df = in_iamdf.pivot_table(
        index=SCENARIO_ASSESSMENT_IDX,
        columns=["variable"],
        aggfunc="sum",
    ).reset_index()
    pivot_df.columns.name = None

    return pivot_df


def _extract_variable_zero_scenarios(in_df, variable):
    def _variable_zero(r):
        valid_rows = (
            np.array([
                not isinstance(v, str)
                for v in r[variable]
            ])
            & np.array([
                not isinstance(v, str)
                for v in r["Surface Temperature"]
            ])
        )

        return (
            (r[valid_rows][variable] <= 0)
            & (r[valid_rows]["Surface Temperature"] > 0)
        ).any()

    extract_idx = list(set(SCENARIO_ASSESSMENT_IDX) - {"time"})
    return in_df.groupby(extract_idx).filter(_variable_zero)


def get_zero_variable_scenarios_year_before_zero_variable_peak_temp_thereafter_rows(
    in_iamdf,
    variable,
):
    def _negative_or_zero_variable_positive_surface_temperature_time(r):
        if isinstance(r[variable].values[0], str):
            return False
        if isinstance(r["Surface Temperature"].values[0], str):
            return False
        return (
            # avoid rounding errors with 10**-8
            (r[variable] <= 10**-8)
            & (r["Surface Temperature"] > 0)
        )

    def _positive_variable_emissions_time(r):
        if isinstance(r[variable].values[0], str):
            return False
        return (
            # avoid rounding errors with 10**-8
            (r[variable] > 10**-8)
        )

    tdf = _scenario_assessment_pivot(in_iamdf)
    tdf = _extract_variable_zero_scenarios(tdf, variable)

    valid_rows = np.array([
        not isinstance(v, str)
        for v in tdf["Surface Temperature"]
    ])
    tdf = tdf.loc[valid_rows, :]

    negative_or_zero_emms_positive_surface_temperature_df = tdf.groupby(SCENARIO_ASSESSMENT_IDX).filter(
        _negative_or_zero_variable_positive_surface_temperature_time
    )

    keep_max_temp = negative_or_zero_emms_positive_surface_temperature_df.groupby(
        list(set(SCENARIO_ASSESSMENT_IDX) - {"time"})
    )["Surface Temperature"].transform(max) == negative_or_zero_emms_positive_surface_temperature_df["Surface Temperature"]
    df_peak_temp = negative_or_zero_emms_positive_surface_temperature_df[keep_max_temp].set_index(
        list(set(SCENARIO_ASSESSMENT_IDX) - {"time"})
    )
    # remove if two times have same peak temperature
    keep_first_time = df_peak_temp.groupby(
        list(set(SCENARIO_ASSESSMENT_IDX) - {"time"})
    )["time"].transform(max) == df_peak_temp["time"]
    df_peak_temp = df_peak_temp[keep_first_time]

    positive_emms_df = tdf.groupby(SCENARIO_ASSESSMENT_IDX).filter(
        _positive_variable_emissions_time
    )

    keep_zero_emms = positive_emms_df.groupby(
        list(set(SCENARIO_ASSESSMENT_IDX) - {"time"})
    )["time"].transform(max) == positive_emms_df["time"]
    df_zero_emms = positive_emms_df[keep_zero_emms].set_index(
        list(set(SCENARIO_ASSESSMENT_IDX) - {"time"})
    )

    return {
        "zero_emms": df_zero_emms,
        "peak_temp": df_peak_temp
    }


def wrangle_nans(idf):
    # could avoid much of this if https://github.com/pandas-dev/pandas/issues/3729 were solved
    idx = idf.index.names
    wide = idf.reset_index()

    # see https://github.com/openclimatedata/openscm/issues/154 for why we must do this
    for col in wide:
        if isinstance(col, datetime.datetime):
            continue
        if col == "rf_total_runmodus":  # hack hack hack (might have actually been solved by the use of `any` below...)
            wide[col] = wide[col].astype(str)
        if col == "non_co2_diagnosis":  # hack hack hack (might have actually been solved by the use of `any` below...)
            wide.loc[wide[col].isna(), col] = False

        if wide[col].apply(lambda x: isinstance(x, str)).any():
            wide.loc[wide[col].isna(), col] = "N/A"
            wide.loc[wide[col] == "nan", col] = "N/A"
            wide.loc[wide[col] == "NaN", col] = "N/A"
        else:
            wide.loc[
                wide[col].isna(), col
            ] = -9999  # can't use -999 as then changed back in appends...

    return wide.set_index(idx)


def _get_cumulative_sum_of_variable(bdf, base_var):
    # MAGICC's internal convention is that fluxes represent the mid-point flux
    # for the year whilst state variables are the value as at 1st Jan in the
    # given year. For consistency with temperature and concentrations, we want
    # cumulative emissions, which are a state variable, to follow this
    # convention too. Take cumulative sum. The years here are wrong as, for
    # example, the first year value is non-zero. However, in the first year at
    # 1st Jan, cumulative emissions were zero.
    raw = bdf.filter(variable=base_var).timeseries()
    raw_cumulative = raw.cumsum(axis=1, skipna=False)

    # To fix the year error, shift the years forward, add an extra column with
    # zero at the first year, drop the last year (to avoid making extra columns)
    # and then set any data in the cumulative output to nan which is also nan
    # in the input.
    raw_cumulative.columns = raw_cumulative.columns.map(
        lambda x: x.replace(year=x.year + 1)
    )
    raw_cumulative.loc[
        :,
        datetime.datetime(raw_cumulative.columns.min().year-1, 1, 1)
    ] = 0
    cumulative = raw_cumulative.sort_index(axis=1)
    cumulative = cumulative.drop(cumulative.columns.max(), axis=1)
    cumulative[raw.isna()] = np.nan

    # far simpler once pint is working with pandas
    cumulative = cumulative.reset_index()
    cumulative["variable"] = "Cumulative {}".format(base_var)
    assert all([
        ("/yr" in u) or ("/ yr" in u)
        for u in cumulative["unit"]
    ])
    cumulative["unit"] = cumulative["unit"].apply(
        lambda x: x.replace("/ yr", "").replace("/yr", "").strip()
    )
    cumulative = cumulative.set_index(
        bdf.timeseries().index.names
    )

    return cumulative


def get_aggregate_variable(idf, base_var):
    total = (
        idf.filter(variable="{}|*".format(base_var))
        .timeseries()
        .groupby(list(set(idf.timeseries().index.names) - {"variable"}))
        .apply(pd.DataFrame.sum, skipna=False)
    )
    total = total.assign(variable=base_var).set_index(
        "variable", append=True
    )

    return total


def add_peak_temp_peak_total_cumulative_co2_emissions_to_scmdf(
    indf,
    cumulative_emissions_peak_ref_year=CUMULATIVE_EMISSIONS_REF_YEAR,
    peak_temperature_id_string="(rel. to 2006-2015)",
    include_inverse_co2_emissions=True,
    include_permafrost_emissions=True,
    include_peaks=True,
):
    # Wrangle nans so `groupby` and other operations do sensible things later.
    tdf = ScmDataFrame(wrangle_nans(indf.timeseries()))

    # Calculate total CO2 emissions
    # with pyam 0.2.0, this would be much cleaner
    co2_total = get_aggregate_variable(tdf, "Emissions|CO2")
    tdf.append(co2_total, inplace=True)

    cumulative_co2 = _get_cumulative_sum_of_variable(
        tdf, "Emissions|CO2"
    )
    tdf.append(cumulative_co2, inplace=True)

    if include_inverse_co2_emissions:
        # Calculate total inverse emissions
        inverse_co2_total = get_aggregate_variable(tdf, "Inverse Emissions|CO2")
        tdf.append(inverse_co2_total, inplace=True)

        inverse_cumulative_co2 = _get_cumulative_sum_of_variable(
            tdf, "Inverse Emissions|CO2"
        )
        tdf.append(inverse_cumulative_co2, inplace=True)

    if include_permafrost_emissions:
        # Add cumulative permafrost emissions
        cumulative_co2_permafrost = _get_cumulative_sum_of_variable(
            tdf, "Land to Air Flux|CO2|MAGICC Permafrost"
        )
        tdf.append(cumulative_co2_permafrost, inplace=True)

    if include_peaks:
        # Add peak temperature and peak cumulative emissions
        peak_temperature = (
            tdf.filter(
                variable="Surface Temperature {}".format(peak_temperature_id_string),
                region="World",
            )
            .timeseries()
            .max(axis="columns")
            .to_frame()
        )

        classification_idx = list(
            set(peak_temperature.index.names) - {"region", "variable", "unit"}
        )
        peak_temperature = (
            peak_temperature.reset_index()
            .set_index(classification_idx)
            .drop(["region", "variable", "unit"], axis="columns")[0]
        )
        peak_temp_name = "peak surface temperature {}".format(peak_temperature_id_string)


        peak_temperature_idx_max = (
            tdf.filter(
                variable="Surface Temperature {}".format(peak_temperature_id_string),
                region="World",
            )
            .timeseries()
            .idxmax(axis="columns")
        )

        co2_unit = (
            tdf.filter(variable="Cumulative Emissions|CO2", region="World")
            .timeseries()
            .index.get_level_values("unit")
            .unique()
        )
        assert len(co2_unit) == 1
        co2_unit = co2_unit[0].strip()

        cumulative_emissions_at_peak_temperature = peak_temperature_idx_max.reset_index()
        cumulative_emissions_at_peak_temperature["variable"] = "Cumulative Emissions|CO2"
        cumulative_emissions_at_peak_temperature["unit"] = co2_unit
        cumulative_emissions_at_peak_temperature = cumulative_emissions_at_peak_temperature.set_index(
            peak_temperature_idx_max.index.names
        )[0]

        cumulative_emissions_at_peak_temperature = pd.Series(
            tdf.timeseries().lookup(
                cumulative_emissions_at_peak_temperature.index,
                cumulative_emissions_at_peak_temperature.values
            ),
            cumulative_emissions_at_peak_temperature.index,
        )

        cumulative_emissions_ref = (
            tdf.filter(
                variable="Cumulative Emissions|CO2",
                region="World",
                year=cumulative_emissions_peak_ref_year,
            )
            .timeseries()
        )
        assert cumulative_emissions_ref.shape[1] == 1
        cumulative_emissions_at_peak_temperature -= cumulative_emissions_ref.iloc[:, 0]
        cumulative_emissions_at_peak_temperature = cumulative_emissions_at_peak_temperature.to_frame(
        ).reset_index(
        ).set_index(
            classification_idx
        ).drop(["region", "variable", "unit"], axis="columns")[0]
        peak_temperature_idx_max = peak_temperature_idx_max.to_frame().reset_index().set_index(classification_idx).drop(["region", "variable", "unit"], axis="columns")[0]

        cumulative_emms_at_peak_temp_name = "cumulative emissions at peak temperature from {} ({})".format(
            cumulative_emissions_peak_ref_year, co2_unit
        )

        peak_cumulative_emissions = (
            tdf.filter(variable="Cumulative Emissions|CO2", region="World")
            .timeseries()
            .max(axis="columns")
        )
        peak_cumulative_emissions -= cumulative_emissions_ref.iloc[:, 0]
        peak_cumulative_emissions = peak_cumulative_emissions.to_frame(
        ).reset_index(
        ).set_index(
            classification_idx
        ).drop(["region", "variable", "unit"], axis="columns")[0]

        peak_cumulative_emms_name = "peak cumulative emissions from {} ({})".format(
            cumulative_emissions_peak_ref_year, co2_unit
        )

        peak_cumulative_emissions_idx_max = (
            tdf.filter(
                variable="Cumulative Emissions|CO2",
                region="World",
            )
            .timeseries()
            .idxmax(axis="columns")
        )

        temperature_unit = (
            tdf.filter(variable="Surface Temperature", region="World")
            .timeseries()
            .index.get_level_values("unit")
            .unique()
        )
        assert len(temperature_unit) == 1
        temperature_unit = temperature_unit[0].strip()

        temperature_at_peak_cumulative_emissions = peak_cumulative_emissions_idx_max.reset_index()
        temperature_at_peak_cumulative_emissions["variable"] = "Surface Temperature {}".format(peak_temperature_id_string)
        temperature_at_peak_cumulative_emissions["unit"] = temperature_unit
        temperature_at_peak_cumulative_emissions = temperature_at_peak_cumulative_emissions.set_index(
            peak_cumulative_emissions_idx_max.index.names
        )[0]

        temperature_at_peak_cumulative_emissions = pd.Series(
            tdf.timeseries().lookup(
                temperature_at_peak_cumulative_emissions.index,
                temperature_at_peak_cumulative_emissions.values
            ),
            temperature_at_peak_cumulative_emissions.index,
        )
        temperature_at_peak_cumulative_emissions_name = "surface temperature at peak cumulative emissions {}".format(peak_temperature_id_string)
        temperature_at_peak_cumulative_emissions = temperature_at_peak_cumulative_emissions.to_frame(
        ).reset_index(
        ).set_index(
            classification_idx
        ).drop(["region", "variable", "unit"], axis="columns")[0]
        peak_cumulative_emissions_idx_max = peak_cumulative_emissions_idx_max.to_frame().reset_index().set_index(classification_idx).drop(["region", "variable", "unit"], axis="columns")[0]

        # if not already in there, put this in pyam, see https://github.com/IAMconsortium/pyam/issues/237
        res_with_peaks = []

        ts = tdf.timeseries().reset_index()
        for label, df in ts.groupby(peak_temperature.index.names):
            adf = df.copy()
            adf[peak_temp_name] = peak_temperature[label]
            adf[cumulative_emms_at_peak_temp_name] = cumulative_emissions_at_peak_temperature[label]
            adf[peak_cumulative_emms_name] = peak_cumulative_emissions[label]
            adf[temperature_at_peak_cumulative_emissions_name] = temperature_at_peak_cumulative_emissions[label]
            adf["carbon budget temperature {}".format(peak_temperature_id_string)] = peak_temperature[label]

            # peak temp occurs after maximum emissions i.e. temperature still rising
            # as we hit net zero
            if peak_temperature_idx_max[label] > peak_cumulative_emissions_idx_max[label]:
                adf[
                    "carbon budget emissions from {} ({})".format(
                        cumulative_emissions_peak_ref_year, co2_unit
                    )
                ] = cumulative_emissions_at_peak_temperature[label]
            else:
                adf[
                    "carbon budget emissions from {} ({})".format(
                        cumulative_emissions_peak_ref_year, co2_unit
                    )
                ] = peak_cumulative_emissions[label]

            adf.columns.name = None

            res_with_peaks.append(adf)

        tdf = ScmDataFrame(pd.concat(res_with_peaks))

    return ScmDataFrame(tdf.timeseries())


def get_1_percent_tcre_df(in_scmdf, tuning="default", tuning_col="carbon_cycle_tuning"):
    df = in_scmdf.filter(
        variable=[
            "*Inverse*CO2",
            "Atmos*Conc*CO2",
            "Radiative Forcing",
            "Surface Temperature*",
        ],
        region="World",
        year=range(1850, 2001),
        scenario="1%/yr CO2"
    ).pivot_table(
        index=["time", "model", "scenario", "climate_model", tuning_col],
        columns=["variable"],
        aggfunc="sum",
    )
    df.columns.name = None
    df.reset_index(inplace=True)
    df = df[df[tuning_col] == tuning]

    return df[[
        "time",
        "Atmospheric Concentrations|CO2",
        "Inverse Emissions|CO2",
        "Cumulative Inverse Emissions|CO2",
    #     "Radiative Forcing",
        "Surface Temperature",
    ]]


def get_co2_doubling_row(one_percent_tcre_df):
    co2_conc_doubling_idx = one_percent_tcre_df["Atmospheric Concentrations|CO2"].idxmax()
    return one_percent_tcre_df.loc[co2_conc_doubling_idx, :]


def convert_to_gtco2(idf):
    ts = idf.timeseries()
    ts_ri = ts.reset_index()
    convert_rows = (ts_ri["unit"] == "Gt C / yr").values
    ts.loc[convert_rows, :] *= unit_registry("C").to("CO2").magnitude
    ts = ts.reset_index()
    ts.loc[convert_rows, "unit"] = "Gt CO2 / yr"
    res = ScmDataFrame(ts)
    assert "Gt C / yr" not in res["unit"].unique()

    return res
