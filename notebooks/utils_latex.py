def new_latex_command(name, value):
    # don't use numbers or hyphens or underscores
    return (
        "\\newcommand{\\"
        + "{}".format(name)
        + "}{"
        + "{}".format(value)
        + "}"
    ).replace("CO2", "CO\\textsubscript{2}")
