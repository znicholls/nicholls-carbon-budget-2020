import string

from matplotlib.colors import to_rgba_array

SCATTER_UNDER_LINE_ALPHA = 0.5


SAVE_KWARGS = {
    "bbox_inches": "tight",
    "pad_inches": 0,
    "transparent": True,
}

COLOURS = {
    "online-calculation": "#00BFFF",
    "TCRE": "#F18805",
    "TCRE-accent": "#F1CC05",
    "scenarios-diagnosis": "#008813",
    "scenarios-diagnosis-accent": "#00ff00",
    "scenarios-scaled": "#471323",
    "scenarios-scaled-accent": "#A42E8b",
    "non-CO2": "#D95D39",
    "non-CO2-highlight-1": "#274D39",
    "non-CO2-highlight-2": "#1AAD39",
    "ZEC": "#581F18",
    "ZEC-accent": "#B51621",
    "unrepresented-process": "#043C6B",
    "unrepresented-process-accent": "#04696B",
    "white": "#FFFFFF",
    "black": "#000000",
}

# write out GIMP palette for inkscape
with open("figures_palette.gpl", "w") as f:
    f.write("GIMP Palette\n")
    f.write("Name: nicholls_carbon_budget_2019\n")
    f.write("#\n")
    for _, c in COLOURS.items():
        rgb = [d * 255 for d in to_rgba_array(c)[0][:3]]
        f.write("{:3.0f} {:3.0f} {:3.0f} {}\n".format(*rgb, c))


def setup():
    import seaborn as sns
    from pandas.plotting import register_matplotlib_converters
    import matplotlib

    register_matplotlib_converters()

    figsize = (7, 6)
    sns.set(rc={"figure.figsize": figsize})
    matplotlib.rcParams["figure.figsize"] = figsize

    # thank you http://jonathansoma.com/lede/data-studio/matplotlib/exporting-from-matplotlib-to-open-in-adobe-illustrator/
    matplotlib.rcParams["pdf.fonttype"] = 42  # avoid Type 3 fonts
    matplotlib.rcParams["ps.fonttype"] = 42  # avoid Type 3 fonts

    # output text properly thank you https://stackoverflow.com/a/50563208
    matplotlib.rcParams["svg.fonttype"] = "none"
    matplotlib.rcParams["text.usetex"] = False

    # set font to match ERL requirements https://publishingsupport.iopscience.iop.org/authoring-for-journals/
    matplotlib.rcParams["font.family"] = "sans-serif"
    matplotlib.rcParams["font.sans-serif"] = ["Arial"]


    matplotlib.rcParams["lines.linewidth"] = 2
    matplotlib.rcParams["axes.facecolor"] = "white"
    matplotlib.rcParams["axes.edgecolor"] = "black"
    matplotlib.rcParams["xtick.bottom"] = True
    matplotlib.rcParams["ytick.left"] = True
    # make sure grid lines appear if they're on
    matplotlib.rcParams["grid.color"] = "black"
    matplotlib.rcParams["axes.grid"] = False


    matplotlib.rcParams["font.size"] = 12

    matplotlib.rcParams["axes.labelsize"] = 12
    matplotlib.rcParams["axes.labelweight"] = "bold"

    matplotlib.rcParams["xtick.labelsize"] = 10
    matplotlib.rcParams["ytick.labelsize"] = 10

    matplotlib.rcParams["axes.titlesize"] = 16
    matplotlib.rcParams["figure.titleweight"] = "bold"

    matplotlib.rcParams["legend.fontsize"] = 10
    matplotlib.rcParams["legend.handlelength"] = 2


def show_all_matplotlib_fonts_in_notebook():
    import matplotlib.font_manager
    from IPython.core.display import HTML

    def make_html(fontname):
        return "<p>{font}: <span style='font-family:{font}; font-size: 24px;'>{font}</p>".format(font=fontname)

    code = "\n".join([
        make_html(font)
        for font in sorted(
            set([
                f.name
                for f in matplotlib.font_manager.fontManager.ttflist
                ])
            )
    ])

    return HTML("<div style='column-count: 2;'>{}</div>".format(code))


def get_annotation_for_panel_number(k):
    return "({})".format(string.ascii_lowercase[k])
